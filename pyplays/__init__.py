# This file is part of pyplays and is licenced under the MPLv2.0
# (c) 2021 David Koňařík
import os
import keyword
from ansible.plugins.loader import action_loader, module_loader

# _ansible_data wouldn't be imported on * otherwise
__all__ = ["_ansible_data", "add_toplevel", "import_playbook", "Block", "Play", "Tasks", "Args"]

# XXX: Needs to be cleared before/after each playbook!
# Done by pyplays_launcher.py
_ansible_data = []

def _filter_dict(d):
    return {k: v for (k, v) in d.items() if v is not None}

def add_toplevel(data):
    _ansible_data.append(data)

def import_playbook(path, name=None, **kwargs):
    add_toplevel(_filter_dict(
        {"import_playbook": path, "vars": kwargs, "name": name}))

class TaskBuilder:
    def __init__(self):
        self.tasks = []

    def task(self, module, _name=None, _free_form=None, _delegate_to=None,
             _vars=None, _tags=None, _register=None, _when=None, _become=None,
             _become_user=None, **kwargs):
        if _free_form is not None and kwargs != {}:
            raise Exception("Tried to both pass named arguments and a free-form argument!")
        kwargs_new = {}
        for key, value in kwargs.items():
            if key.endswith("_") and key[:-1] in keyword.kwlist:
                key = key[:-1]
            kwargs_new[key] = value

        temp = {
            module: _free_form or kwargs_new,
            "name": _name,
            "delegate_to": _delegate_to,
            "vars": _vars,
            "tags": _tags,
            "register": _register,
            "when": _when,
            "become": _become,
            "become_user": _become_user,
        }
        self.tasks.append(_filter_dict(temp))

plugin_paths = \
        list(action_loader.all(path_only=True)) \
      + list(module_loader.all(path_only=True)) \
      + ["meta", "include", "include_tasks", "include_role", "import_tasks", "import_role"]

for plugin_path in plugin_paths:
    mod_name = os.path.basename(plugin_path).replace(".py", "")
    if hasattr(TaskBuilder, mod_name): continue
    func_name = mod_name
    if func_name in keyword.kwlist:
        func_name += "_"
    setattr(TaskBuilder, func_name,
            (lambda mod:
                lambda self, _name=None, _free_form=None, **kwargs:
                    self.task(mod, _name=_name, _free_form=_free_form, **kwargs)
            )(mod_name))

class Block(TaskBuilder):
    def __init__(self, play, _name=None, _delegate_to=None, _vars=None,
            _tags=None, _when=None, _become=None, _become_user=None,):
        super().__init__()
        self.play = play
        self.kwargs = dict(
            _name=_name, _delegate_to=_delegate_to, _vars=_vars, _tags=_tags,
            _when=_when, _become=_become, _become_user=_become_user)

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.play.task("block", _free_form=self.tasks, **self.kwargs)

class Play(TaskBuilder):
    def __init__(
            self, hosts, remote_user=None, become=None, become_user=None,
            become_method=None, vars=None, tags=None):
        super().__init__()
        self._template = {
            "tasks": [],
            "hosts": hosts,
            "remote_user": remote_user,
            "become": become,
            "become_user": become_user,
            "become_method": become_method,
            "vars": vars,
            "tags": tags,
        }

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self._template["tasks"] = self.tasks
        add_toplevel(_filter_dict(self._template))

class Tasks(TaskBuilder):
    def __init__(self):
        super().__init__()

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        for task in self.tasks: add_toplevel(task)

class Args:
    class ArgNotFoundException(Exception):
        def __init__(self, arg_name):
            super().__init__(f"Required argument not found: {arg_name}")

    class UnexpectedArgException(Exception):
        def __init__(self, arg_name):
            super().__init__(f"Got unexpected argument: {arg_name}")

    def __init__(self, vars, arg_defs):
        self.vars = vars
        self.arg_names = []
        for arg in arg_defs:
            if isinstance(arg, str):
                if arg not in vars:
                    raise Args.ArgNotFoundException(arg)
                self.arg_names.append(arg)
            else:
                if arg[0] not in vars:
                    vars[arg[0]] = arg[1]
                self.arg_names.append(arg[0])

    def __getattr__(self, name):
        if name not in self.arg_names:
            raise Exception(f"Tried to access unregistered argument {name}")
        return self.vars[name]
