#!/usr/bin/env python3
# This file is part of pyplays and is licenced under the MPLv2.0
# (c) 2022 David Koňařík
# Parts of this file have been adapted from the Ansible source code and may be
# licenced differently
from collections.abc import Mapping
import os
import sys
import runpy
import types
import ansible.playbook.helpers
from ansible import constants as C
from ansible.parsing.dataloader import DataLoader
from ansible.errors import AnsibleParserError
from ansible.plugins.loader import add_all_plugin_dirs
from ansible.playbook.play import Play
from ansible.module_utils._text import to_native, to_text
from ansible.playbook.playbook_include import PlaybookInclude
from ansible.playbook import Playbook, display
from ansible.playbook.role import Role
from ansible.utils.path import is_subpath, unfrackpath
from ansible.utils.vars import combine_vars


def run_python_file(code, filename, vars):
    pb_locals = {}
    pb_globals = {"vars": vars or {}}
    c = compile(code, filename, "exec")
    exec(c, pb_globals, pb_locals)

    import pyplays

    data = pb_locals.get("_ansible_data") \
            or pb_globals.get("_ansible_data") \
            or pyplays._ansible_data

    # Clear for future playbooks
    pyplays._ansible_data = []

    return data


load_list_of_blocks_old = ansible.playbook.helpers.load_list_of_blocks
load_list_of_blocks_old = types.FunctionType(
        load_list_of_blocks_old.__code__,
        load_list_of_blocks_old.__globals__)
def load_list_of_blocks_new(ds, play, parent_block=None, role=None, task_include=None, use_handlers=False, variable_manager=None, loader=None):
    if len(ds) == 1 and "python" in ds[0]:
        assert variable_manager
        vars = variable_manager.get_vars(play=play, task=task_include)
        ds = run_python_file(ds[0]["python"]["code"], ds[0]["python"]["filename"], vars)
    return load_list_of_blocks_old(ds, play, parent_block, role, task_include, use_handlers, variable_manager, loader)
ansible.playbook.helpers.load_list_of_blocks.__code__ = load_list_of_blocks_new.__code__
ansible.playbook.helpers.load_list_of_blocks.__globals__["run_python_file"] = run_python_file
ansible.playbook.helpers.load_list_of_blocks.__globals__["load_list_of_blocks_old"] = load_list_of_blocks_old


load_from_file_old = DataLoader.load_from_file
def load_from_file_new(self, file_name, *args, **kwargs):
    if isinstance(file_name, bytes):
        file_name = file_name.decode()
    if file_name.endswith(".py"):
        file_name = self.path_dwim(file_name)
        display.debug("Loading data from %s" % file_name)

        (b_file_data, _) = self._get_file_contents(file_name)

        file_data = to_text(b_file_data, errors='surrogate_or_strict')
        parsed_data = [{
            "python": {
                "code": file_data,
                "filename": os.path.abspath(file_name),
            },
        }]

        # cache the file contents for next time
        self._FILE_CACHE[file_name] = parsed_data
        return parsed_data
    else:
        return load_from_file_old(self, file_name, *args, **kwargs)
DataLoader.load_from_file = load_from_file_new


load_playbook_data_old = Playbook._load_playbook_data
# What we need to do is change a few lines in Playbook._load_playbook_data
# Unfortunately, those lines are in the middle, so for ".py" files, we
# basically divert the function into out slightly modified copy
def load_playbook_data_new(self, file_name, variable_manager, vars=None):
    if file_name.endswith(".py"):
        if os.path.isabs(file_name):
            self._basedir = os.path.dirname(file_name)
        else:
            self._basedir = os.path.normpath(os.path.join(self._basedir, os.path.dirname(file_name)))

        # set the loaders basedir
        cur_basedir = self._loader.get_basedir()
        self._loader.set_basedir(self._basedir)

        add_all_plugin_dirs(self._basedir)

        self._file_name = file_name

        code_bytes = self._loader._get_file_contents(os.path.basename(file_name))[0]
        try:
            code = code_bytes.decode()
        except UnicodeDecodeError as e:
            raise AnsibleParserError("Could not read playbook (%s) due to encoding issues: %s" % (file_name, to_native(e)))

        ds = run_python_file(code, os.path.abspath(file_name), vars)

        # check for errors and restore the basedir in case this error is caught and handled
        if ds is None:
            self._loader.set_basedir(cur_basedir)
            raise AnsibleParserError("Empty playbook, nothing to do: %s" % unfrackpath(file_name), obj=ds)
        elif not isinstance(ds, list):
            self._loader.set_basedir(cur_basedir)
            raise AnsibleParserError("A playbook must be a list of plays, got a %s instead: %s" % (type(ds), unfrackpath(file_name)), obj=ds)
        elif not ds:
            self._loader.set_basedir(cur_basedir)
            raise AnsibleParserError("A playbook must contain at least one play: %s" % unfrackpath(file_name))

        # Parse the playbook entries. For plays, we simply parse them
        # using the Play() object, and includes are parsed using the
        # PlaybookInclude() object
        for entry in ds:
            if not isinstance(entry, dict):
                # restore the basedir in case this error is caught and handled
                self._loader.set_basedir(cur_basedir)
                raise AnsibleParserError("playbook entries must be either valid plays or 'import_playbook' statements", obj=entry)

            if any(action in entry for action in C._ACTION_IMPORT_PLAYBOOK):
                pb = PlaybookInclude.load(entry, basedir=self._basedir, variable_manager=variable_manager, loader=self._loader)
                if pb is not None:
                    self._entries.extend(pb._entries)
                else:
                    which = entry
                    for k in C._ACTION_IMPORT_PLAYBOOK:
                        if k in entry:
                            which = entry[k]
                            break
                    display.display("skipping playbook '%s' due to conditional test failure" % which, color=C.COLOR_SKIP)
            else:
                entry_obj = Play.load(entry, variable_manager=variable_manager, loader=self._loader, vars=vars)
                self._entries.append(entry_obj)

        # we're done, so restore the old basedir in the loader
        self._loader.set_basedir(cur_basedir)
    else:
        load_playbook_data_old(self, file_name, variable_manager, vars)
Playbook._load_playbook_data = load_playbook_data_new

load_role_yaml_old = Role._load_role_yaml
def load_role_yaml_new(self, subdir, main=None, allow_dir=False):
    data = None
    file_path = os.path.join(self._role_path, subdir)
    if self._loader.path_exists(file_path) and self._loader.is_directory(file_path):
        # Valid extensions and ordering for roles is hard-coded to maintain portability
        extensions = ['.py', '.yml', '.yaml', '.json']  # same as default for YAML_FILENAME_EXTENSIONS

        # look for files w/o extensions before/after bare name depending on it being set or not
        # keep 'main' as original to figure out errors if no files found
        if main is None:
            _main = 'main'
            extensions.append('')
        else:
            _main = main
            extensions.insert(0, '')

        # not really 'find_vars_files' but find_files_with_extensions_default_to_yaml_filename_extensions
        found_files = self._loader.find_vars_files(file_path, _main, extensions, allow_dir)
        if found_files:
            for found in found_files:

                if not is_subpath(found, file_path):
                    raise AnsibleParserError("Failed loading '%s' for role (%s) as it is not inside the expected role path: '%s'" %
                                             (to_text(found), self._role_name, to_text(file_path)))


                if found.endswith(".py"):
                    code_bytes = self._loader._get_file_contents(found)[0]
                    try:
                        code = code_bytes.decode()
                    except UnicodeDecodeError as e:
                        raise AnsibleParserError("Could not read playbook (%s) due to encoding issues: %s" % (found, to_native(e)))
                    vars = self._variable_manager.get_vars(play=self._play)
                    new_data = run_python_file(code, os.path.abspath(found), vars)
                else:
                    new_data = self._loader.load_from_file(found)
                if new_data:
                    if data is not None and isinstance(new_data, Mapping):
                        data = combine_vars(data, new_data)
                    else:
                        data = new_data

                    # found data so no need to continue unless we want to merge
                    if not allow_dir:
                        break

        elif main is not None:
            # this won't trigger with default only when <subdir>_from is specified
            raise AnsibleParserError("Could not find specified file in role: %s/%s" % (subdir, main))

    return data
Role._load_role_yaml = load_role_yaml_new


def main():
    if len(sys.argv) < 2:
        print("Usage: pyplays_launcher.py <ansible command> ...", file=sys.stderr)
        sys.exit(1)

    runpy.run_module("ansible", {}, "__main__")


if __name__ == "__main__":
    main()
